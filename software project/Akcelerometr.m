clear all;
load('data.mat');

x = p7;

for q=1:3
  
    xin=x(:,q);
end;


N=50; % pocet vzorku
%normalized frequency

Wn=0.00000000001 % vzorkovaci frekvence

B = fir1(N,Wn); %navrhe FIR filtr doplni propust

  psd(B) % nakresli amplitudovou charku - podle toho poladit N, Wn. 
    
  for q=1:3
  
    xin=x(:,q);
    
    dopln=200;
    xi=[zeros(1,dopln) xin' zeros(1,dopln)];
    
    xf2=filter(B,1,xi);  % tohle dela vlastni filtrace.

    % move the signal backward due to lag of N/2
    xf2=[xf2(N/2:length(xf2)) ones(1,N/2)*xf2(length(xf2))];
    xf2(1:N)=mean(x(1:N));
    
    xf2=xf2(dopln+1:length(xf2)-dopln-1); 
    
    %xf2=xin;
    
    xout(:,q)=xf2;
        
    
  end;
    figure
    subplot(2,1,1)
    plot(x);
    subplot(2,1,2)
    plot(xout);
    
    legend('x','y','z');