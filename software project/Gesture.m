% Script for recognazing two gestures
%  - move left and back
%  - move right and back

load data.mat;

data = p1_filtered;
tolerance = 1;

x = data(:,1); % We care only about X axis
figure;
plot(x);

[pks] = findpeaks(x);

pks = pks(pks>1);

if size(pks,1) == 1
    disp('Recognized gesture "left and back"');
elseif size(pks,1) == 2
    disp('Recognized gesture "right and back"');
else
    disp('Unkown gesture');
end;

