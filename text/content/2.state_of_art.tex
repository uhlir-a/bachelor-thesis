
\chap State of the art

\sec KNX

\secc Overview

KNX is the world wide standard for building management. It is the successor of BatiBUS, EIB and EHS. It was standardized as an International Standard (ISO/IEC 14543-3), a European Standard (CENELEC EN 50090 and CEN EN 13321-1) and as a Chinese Standard (GB/T 20965) \cite[WhatIsKnx]. It is a decentralized network which uses the bus concept for creating networks with advanced elements, which communicate with each other. For communication, data telegrams are used. \cite[AbbKnx]

KNX uses a layer system similar to the OSI model. From 7 OSI layers, only 5 are used. The comparison can be seen in Figure \ref[OSIvsKNX].

\midinsert \clabel[OSIvsKNX]{Comparation of OSI and KNX layers}
\picw=7cm \cinspic imgs/osi_vs_knx.png
\caption/f Comparation of OSI and KNX layers, \cite[Wolfgang2008]
\endinsert

One of the differences from normal flat installation is flexibility. The KNX network can be more easily reconfigured. For example, when a change of a switch target needs to be made, from hallway lights to room lights, no construction is needed, a software reconfiguration will be enough. Moreover, intelligent learning mechanisms can be applied to the KNX network. These mechanisms will observe user behavior and based on discovered patterns it will modify its own behavior. For example when the user is not at home during the day, the heater will lower the temperature and before the user returns the heater will increase the temperature to a desired level. Also the KNX network requires less cabling then normal flat installations. An example of the difference between the KNX installation and a normal flat installation can be seen in Figure \ref[KNXdifferences].

\midinsert \clabel[KNXdifferences]{Differences between normal and KNX installation}
\picw=11cm \cinspic imgs/intelligent_installation.png
\caption/f Differences between normal and KNX installation, \cite[AbbKnx]
\endinsert

\secc Installation

The KNX installation uses a different media. In KNX technology two types of lines are distinguished --- the bus line and the power line. The bus line maintains the data flow and it can be said, that the controlling of the Smart Home is done through this line. The power line is, on the other hand, supplying energy for element's functions (eq. lights, heating and so on). Some elements do not have high power requirements and therefore they can be powered directly from the bus line.

As mentioned in \cite[Wolfgang2008], there are 4 types of media for a bus line --- twisted pair, power line, radio and Ethernet.

\seccc Twisted pair

\begitems
* TP--0 (type 0) --- It posses data throughput 4,8 bit/s and originally is taken from BatiBUS. 
* TP--1 (type 1) --- It is originally taken from EIB and it posses data throughput 9,6 bit/s.
\enditems

The most common media for installation is TP--1. It carries 29V DC voltage and allows up to 1000m per physical segment. It also allows usage of line repeaters, which can be used maximally 4 times forming 4 segments in a 4 000m long line.

\seccc Power line

\begitems
* PL--110 (110 kHz) --- As TP--1, this medium was taken from EIB, the data throughput is 1,2 kbit/s.
* PL--132 (132 kHz) --- It is originally taken from EHS and it posses data throughput of 2,4 kbit/s.
\enditems

\seccc Radio

Another medium is radio on frequency 868 MHz which was developed as part of the KNX wireless standard. It can transmit up to 38,4 kbit/s.

\seccc Ethernet

The KNX standard supports Ethernet media as well. This media can be used together with {\em KNX over IP} specification. Usually an extra element which acts as a bridge between the Ethernet and other network media is needed.

\secc Topology

The KNX network is very flexible regarding network topology as stated in \cite[AbbKnx]. The only constraint is is that this network can not contain loops. Besides that we can use topology models such as tree topology, star topology or line topology --- examples of these topologies can be seen on Figure \ref[topologies].

\clabel[topologies]{Examples of usable topologies.}	
\centerline {\picw=4.2cm \inspic imgs/tree_topology.png \hfil
             \picw=4.1cm \inspic imgs/line_topology.png \hfil
             \picw=3.1cm \inspic imgs/star_topology.png }\nobreak
\centerline {a)\hfil\hfil b)\hfil\hfil c)}\nobreak\medskip
\caption/f Examples of usable topologies.

The basic unit of KNX network topology is a {\em line}. Every line needs to have its own power supply. If we consider the most common TP--1 media, 64 elements can be placed on a line. According to \cite[AbbKnx] there are some limitations for placement of elements on a line. 

\begitems
* The maximum total length of one line without using repeaters can be 1 000 meters. 
* The maximum distance between the power supply and the last element must be 350 meters. 
* The maximum distance between two elements must be 700 meters. 
* The minimum distance between two power supplies must be 200 meters.
\enditems

If there is a demand for a higher number of elements connected to the network, lines can be connected with a {\em area line}, which can group together up to 15 lines forming an {\em area line}. In a network there can be maximally 15 area lines.

Therefore the maximum number of elements connected to the network can be up to 14 400 elements. If further measures would be applied, the maximum number of elements on each line can be extended to up to 255 elements, which would extend the maximum number of elements to 57 375 elements in total  (according to \cite[AbbKnx]).

\secc Addresses

In the KNX technology two types of addresses are used — physical addresses and group addresses.

\seccc Physical addresses

Every element connected to the KNX network has its own unique physical address. This address is meant for unambiguous addressing of elements inside the network. The physical address is related to the topology of this KNX network, it is 16 bits long and consists of 3 parts — the area address (4 bits), the line address (4 bits) and the position address (8 bits). An example of a physical address can be seen in Figure  \ref[KNXPhysicalAddresses].

Physical addresses are not used in normal traffic, they are used for configuration of the KNX network.

\midinsert \clabel[KNXPhysicalAddresses]{Example of physical address}
\picw=14cm \cinspic imgs/knx_physical_addresses.png
\caption/f Example of physical address, \cite[AbbKnx]
\endinsert

\seccc Group addresses

Group address numbering is based on elements function differentiation. The same as Physical addresses, group addresses are 16 bits long and are divided into 3 groups --- main group (4 bits), middle group (3 bits) and lower group (8 bits). The 16th bit is saved as a broadcasting address. When a  telegram is sent to a broadcast address, all elements in the network will receive it. The division of elements into groups is not standardized, it is up to the designer to set rules according to how those elements will be grouped. One example of Group address interpretation could be: main group / middle group / lower group = room / group function (lighting, heating etc.) / position of element ceiling, wall etc.).

\secc Elements

According to \cite[AbbKnx], elements which are connected to the KNX network can be divided into 4 general groups based on their function.

\begitems
* System devices --- power supplies, communication interfaces (serial interface RS-232, USB, IP), connectors, choke, line and area couplers.
* Sensors --- push buttons, transducers (wind, rain, light, heat, etc.), thermostats, analogue inputs, analogue and binary inputs.
* Actuators --- switching actuators, dimming actuators, actuators for blinds, heating actuators.
* Controllers --- logic units, logic modules.  
\enditems 

System devices are elements which build the infrastructure of the network. They can provide bridges to other technologies, for example older standards such as EHS. Moreover, they can extend the network with elements which are able to provide new types of connectivity such as WiFi, Bluetooth and so on.

Sensors report the state of the Smart Home. They are the ones that start actions, because of the outside inputs. They share this data across the network with other elements.

Actuators execute tasks in the Smart Home. They receive data from sensors, from external networks and based on this data, they evaluate their own further actions.

Controllers are elements which make the network intelligent. They process data from the network and make more complex analysis for more complex functions and decisions.

\label[secControllingKnx] \secc User control interfaces

The most common control interface is taken from a normal home installation. It consists of switches and regulators. These control elements are placed around whole Smart Home. Also these elements can be accompanied with more advanced elements, such as noise listener (for clapping) or motion detectors.

Another quite common control interface of KNX standard is control panels. These panels can consist of touch screens, but there are types of panels which do not have touch screens and have physic control elements. Usually panels are static control devices placed in one place. This interface can be accompanied with remote controls.

\midinsert \clabel[rti]{RTI remote control}
\picw=9cm \cinspic imgs/rti_control.jpg
\caption/f RTI remote control which can be used for Smart Home control
\endinsert

Another control interface takes advantage of external connectivity of the KNX standard such as {\em KNX over IP}. These are smartphone and tablets applications which let you have an overview of your home and also control it. This is possible even when the user is not home (in case that KNX is connected to Internet).

\sec Hand gestures

Defining a hand gesture is not an easy job. It can be perceived on a physical level as the motion of muscles forming the trajectory of a hand. But this perception is lacking context. Usually a gesture is a supporting communication process adding more stress or importance to currently stated information.

\secc Structure of gesture

Every gesture is a dynamic process which has its own structure and therefore it can be divided into several phases. As mentioned in \cite[Huang1997] for Human–Computer Interaction (HCI) purposes, we can assume 3 phases of gesture: 

\begitems
 * preparation
 * stroke
 * retraction 
\enditems

In the the preparation phase, the hand is changing position from its resting position to a position where the stroke will start. The stroke is the main phase, where the gesture is actually performed. It is also part of the movement, that expresses the information of the gesture. Then the retraction follows, which either moves the hand into the resting position, or to a starting position for the next gesture.

\secc Type of gestures

To have an overview of what hand gestures are most frequently used for, lets have a look into the taxonomy presented in \cite[Huang1997]. This taxonomy was developed for the HCI point of view and you can see its overview in Figure  \ref[taxonomy].

\midinsert \clabel[taxonomy]{Taxonomy of hand gesture}
\picw=11cm \cinspic imgs/taxonomy.png
\caption/f Taxonomy of hand gesture, \cite[Huang1997]
\endinsert

Generally, hand movements are divided into two categories --- unintentional movements and gestures. 

Unintentional movements are the ones, which do not contain any information. Gestures are then divided into manipulative and communication gestures.
Manipulative gestures are used for manipulating objects in the real word, for example taking a cup of tea and bringing it to the lips. Communication gestures  usually accompany speech and have the purpose of communication. These are divided into the last two categories --- acts and symbols. Acts are gestures within which information is related directly to the movement. They can be mimetic (imitating some action) or dietic (pointing acts). Symbols, on other hand, are connected to linguistic roles.

\label[secGesturesClasification] \secc Classification of hand gestures

There are several ways to approach the classification of hand gestures. They can be generally divided into two categories --- visual and non--visual based on data from which gestures are classified.

\seccc Visual based classifications


As the name suggests, visual based classification uses image analyses to determine hand position or a hand gesture from images (or footages). This approach evolved tremendously during the past decades, because it utilized a reduction of the size of electronic parts (especially cameras), reducing the price of these parts at the same time increasing the computational power.

One of the products which uses this approach is the XBox Kinect, developed by Microsoft. According to  \cite[Blair2013] it uses an infrared camera alongside the usual RGB camera. The infrared camera emits a 640x480 grid of infrared beams from which a 3D vector is constructed. These data are then used for recognizing the human skeleton in pictures.

Another product which uses this approach and also uses technology similar to Kinect is Leap Motion. Unlike Kinect, Leap Motion focuses only on hand recognition, finger tracking, not the whole human body detection. This minimizing of detection radius positively affected the accuracy of classification. The manufacturer states an accuracy of 0.1 cm, but by experiments with real conditions conducted in \cite[Weichert2013], an accuracy of 0.7 cm was measured. Leap Motion uses several infrared beamers and detectors for recognizing hand and fingers above the device.

\midinsert
\line{\hsize=.5\hsize \vtop{%
      \clabel[kinect]{Image of Kinect camera}
      \picw=4cm \cinspic imgs/kinect.jpg
      \caption/f Image of Kinect camera.
   \par}\vtop{%
      \clabel[leap]{Image of Leap Motion}
      \picw=4cm \cinspic imgs/leap_motion.jpg
      \caption/f Image of Leap Motion.
   \par}}
\endinsert

Technologies which utilize Visual-based classification have already reached a high level of accuracy, which makes them usable in real life. They have, definitely, many benefits. They do not bother the user with peripherals, therefore increasing user comfort. As it was already pointed out, they have a high success rate. But on the other hand they restrict movement of the user, since the place of detection is usually based on the view of a camera(s).

\seccc Non visual based classifications

The second approach was developed mainly in the previous century. It uses mechanical or other contact sensors which measure hand position and from data of these sensors it classifies hand gestures.

One of the old concepts using this approach is called Data Glove. This concept is presented in \cite[Zimmerman1986] and consists of a cotton glove on which flex sensors were attached. Those sensors measured finger angles and hand motion, from the data of these sensors hand gestures and position was calculated. The concept can be seen in Figure \ref[dataglove].

\midinsert \clabel[dataglove]{Image of Data Glove concept}
      \picw=6cm \cinspic imgs/data_glove.jpg
      \caption/f Image of Data Glove concept.
\endinsert

A more modern approach was created by the Canadian startup, Thalmic Labs. They use EMG sensors for detection of muscle activity of our hand and based on this data determine the hand gesture. More detail can be found in Section \ref[secMyo].

The advantage of non-visual based approach is high accuracy, since data usually express the actual state of the hand (for example, the angles between fingers). In general, this method does not limit users in movement around a large area, because the device can be carried
with the person. This is also the main disadvantage, because it can decrease the comfort level for users.

\label[secMyo] \sec Myo

Myo is an armband device developed by Thalmic Labs in Waterloo, Canada. It is a device for hand gesture recognition and arm movement tracking. The goal of Myo is an easy and comfortable control of our PC, smartphones and other products by using hand gestures.

\midinsert \clabel[myo]{Myo armband device.}
\picw=7cm \cinspic imgs/myo.png
\caption/f Myo armband device.
\endinsert

\secc Muscle sensors

For hand gesture detection Myo uses technology based on electromyography (EMG). Finger muscles are attached to the elbow and this fact is used by Myo for its hand gesture detection. Myo is supposed to be placed on the forearm where special EMG sensors monitor the electrical activity of these muscles and from data collected, the type of hand gesture is determined.

Myo contains 8 of these sensors \cite[MyoOfficial], which were specially developed by Thalmic Labs. Because the usual electromyograph is big and expensive, this device is not suitable for such a use case. Moreover, Thalmic Labs was greatly challenged because human anatomy structure differs from one individual to another. Therefore, they developed sensors which are able to overcome issues such as indirect contact with human skin, for example because of hair cover or skin defects. To overcome this issue of human physiology uniqueness, the developers in Thalmic Lab use machine learning algorithms.

\label[secMyoGestures] \secc Gestures and locking mechanism

Because of machine learning algorithms, the set of gestures is limited and from the developer's point of view, not changeable. At the time of writing this thesis (December 2014) Myo possessed five gestures, which can be found in Figure 2.10. Also since Myo is a new device which was officially released in summer of 2014, there is still a lot of development from Thalmic Labs in process. Mostly they are trying to improve the accuracy of their recognition algorithms by releasing new firmware versions, but it is also probable that new gestures will be introduced after current gestures demonstrate suitable reliability.

\midinsert \clabel[myoGestures]{Set of Myo gestures.}
\picw=.2\hsize
\centerline {\inspic imgs/solid_grey_LH_double_tap.png \hfil\hfil \inspic imgs/solid_grey_LH_spread_fingers.png  \hfil\hfil  \inspic imgs/solid_grey_LH_fist.png }\nobreak
\centerline {Double tap gesture \hfil Spread fingers gesture \hfil\hfil  Fist gesture}\nobreak\medskip
\centerline {\inspic imgs/solid_grey_LH_wave_left.png \hfil \inspic imgs/solid_grey_LH_wave_right.png }\nobreak
\centerline {Wave left \hfil Wave right}\nobreak\medskip
\caption/f Set of Myo gestures.
\endinsert

To prevent the misclassification of hand movements as gesture, Myo has a locking mechanism. The mechanism keeps Myo in stand--by mode waiting for only one gesture --- Double tap gesture --- all other gestures are ignored. When the unlocking gesture is made, Myo starts to look for other hand gestures for few seconds. If, in this period, no gesture is made, Myo will lock itself again. This locking mechanism can be modified, disabled or extended from developer's point of view.


\secc Motion sensor and haptic feedback

Myo is also equipped with a nine-axis inertial measurement unit (IMU), which detects arm movement. IMU contains a three-axis gyroscope, a three-axis accelerometer and a three-axis magnetometer. 

Moreover Myo is able to give user haptic feedback with three types of intervals --- short, medium and long vibrations. This enables communication with the user.  For example,  for acknowledging a recognized hand gesture, etc.  

\secc Bluetooth Low Energy

For communication with the external world, Myo uses new Bluetooth standard - Bluetooth Low Energy (or also sometimes referred as Bluetooth Smart).

Bluetooth Low Energy (BLE) was designed to overcome several Bluetooth disadvantages. The goal of Bluetooth is data transfers --- images, footages or any other data. It has a quite high data throughput, the high speed version of the Bluetooth 4 standard has, according to \cite[SparkBT], a data throughput of 24 Mb/s. But the down side of this throughput is high power consumption. 

There are some cases which do not require such a high throughput, for example it needs to communicate only the current status of the device (for example --- door opened/closed). Some cases require the lowest possible power consumption, since it is highly probable that these devices will be battery powered. Specifically for these types of cases, Bluetooth Low Energy was developed. It has throughput 0,27 Mb/s, but it has from 2 to 10 times lower power consumption than the normal Bluetooth standard. This new standard is targeted for the upcoming era of {\em The Internet of things}.

\secc Hardware a API

Because EMG sensors generate a large amount of data, an ARM Cortex M4 processor is used as part of Myo \cite[MyoOfficial]. This processor is used for applying machine learning algorithms to classify executed hand gestures. After a hand gesture is classified, Myo  notifies a pared device through BLE which hand gesture was performed. 

Thalmic Labs provides SDK to access Myos functions. This SDK provides a high level API, that means that developers do not have direct access to raw sensors data, but only to classified hand gestures and motion data from IMU. Currently, Myo SDK  supports Windows 7 and 8, Mac OS, Android and iOS platforms.

\sec Indoor localization

For outdoor localization there is a widely used Global Position System (GPS), but for indoor localization there is no widely used system and there are many approaches to this problem. I will mention several systems and approaches which can be used.

\secc LANDMARC system

The system described in \cite[landmarc] is LANDMARC. This system uses RFID tags and readers for estimation of location. In a place where localization is needed several RFID readers are placed with a stationary network of Reference RFID  tags. Then, to the object which needs to be localized, a Target RFID tag is attached. An example of a set up can be seen in Figure \ref[landmarc]

\midinsert \clabel[landmarc]{Example of LANDMARC set up.}
\picw=8cm \cinspic imgs/landmarc.png
\caption/f Example of LANDMARC set up presented in \cite[landmarc].
\endinsert

Localization is then done through comparing the signal strengths of the Reference tags and the Target tag, assessing the nearest neighbor Reference tag, thereby localizing the Target tag. 

\secc Localization through Inertial sensors

One different approach to indoor localization uses data from inertial sensors, for example, from a smartphone. This approach models our movement through the usage of sensors such as accelerometer, magnetometer and gyroscope. According to \cite[intertial] the accuracy of this approach can be 1,5m if the smartphone is carried in-hand and 2m if the smartphone is carried in a pocket.

\label[RTLS] \secc WiFi RTLS using RSSI
 
 Another approach to indoor localization is Received Signal Strength Indication (RSSI). There are several ways to use this value for estimating the location. One system which uses this approach was developed by Martin Cihlář at the Czech Technical University as a bachelor thesis \cite[RTLSBak] and it is called Real--Time Location System, using the WiFi standard.
  
 This system uses the RSSI fingerprinting method for a network of WiFi Access Points (APs). Across the rooms several WiFi APs are placed. Before actual localization, a collection phase needs to be done, where data points are collected around whole place. These data points are made of RSSI values of all WiFi APs given to the marked location by user. Afterward, the location is estimated by comparing the database of measured data points and therefore estimating the location. As mentioned in the Bachelor thesis \cite[RTLSBak], the average accuracy of their implementation is around 2 meters. 

 \secc Quuppa
 
In Finland there is company called Quuppa, which developed a highly accurate indoor position technology called {\em Intelligent Locating Technology™}. This technology is able to localize a user with 0.1 --- 1 meter accuracy with high refresh rate up to 100Hz. \cite[Quuppa] This technology is based on Nokia's research called High Accuracy Indoor Positioning (HAIP).

For localization they use Bluetooth Low Energy standard. They achieved this accuracy by measuring not the RSSI value, but the angle of the signal arrival. \cite[Quuppa] For this they use special hardware with specially designed antennas for estimating this angle of arrival of the signal. Then most probably by using triangulation, they are able to localize user.  
 